#include "../TrigROBMonitor.h"
#include "../TrigMuCTPiROBMonitor.h"
#include "../TrigALFAROBMonitor.h"
#include "../TrigL1TopoWriteValData.h"
#include "../TrigOpMonitor.h"

DECLARE_COMPONENT( TrigROBMonitor )
DECLARE_COMPONENT( TrigMuCTPiROBMonitor )
DECLARE_COMPONENT( TrigALFAROBMonitor )
DECLARE_COMPONENT( TrigL1TopoWriteValData )
DECLARE_COMPONENT( TrigOpMonitor )
